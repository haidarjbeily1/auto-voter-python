from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class autoVoter:

    def __init__(self) -> None:
        firefox_options = webdriver.FirefoxOptions()
        firefox_options.add_argument('--ignore-certificate-errors')
        firefox_options.add_argument('--ignore-ssl-errors')
        # firefox_options.add_argument('--headless')
        firefox_options.add_argument('--no-sandbox')
        self.driver = webdriver.Firefox(options=firefox_options)

    def make_action(self, _, timeout=5):
        """this function makes some action when passed as a parameter and waits until it's performed"""
        WebDriverWait(self.driver, timeout)

    def get_extensions_page(self):
        self.driver.get('https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/')

    def get_vote_page(self):
        self.driver.get('')

    def add_extension(self):
        xpath = '/html/body/div/div/div/div/div[2]/div[1]/section[1]/div/header/div[4]/div/div/a'
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, xpath)))
        element = self.driver.find_element_by_xpath(xpath)
        self.make_action(element.click(), 60)
    
    def remove_extension(self):
        pass

    def add_wallet(self):
        pass

    def connect_wallet(self):
        pass

    def vote(self):
        pass

    def run(self):
        self.get_extensions_page()
        self.add_extension()
        self.add_wallet()
        self.get_vote_page()
        self.connect_wallet()
        self.vote()
        self.get_extensions_page()
        self.remove_extension()