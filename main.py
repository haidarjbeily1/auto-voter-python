from autoVoter import autoVoter
import time

class Process:
    def __init__(self) -> None:
        self.voter = autoVoter()
    
    def run(self):
        self.voter.run()

def main():
    while True:
        new_process = Process()
        new_process.run()
        time.sleep(3)

if __name__ == '__main__':
    main()